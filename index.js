"use strict";

const diccionario = [
    "perro",
    "tigre",
    "panda",
    "koala",
    "gallo",
    "pollo",
    "mirlo"
]

let palabraSecreta = "perro" //diccionario[random]
let inputList = document.getElementsByClassName("cell");
let tryCounter = 0;
let myTable = document.getElementById("myTable");
let myCellRow = [];

myCellRow = AddRow();

for (const iterator of myCellRow) {
    AddMyEventListener(iterator);
}

function AddMyEventListener(myInput){
    myInput.addEventListener("keydown", function (key) {
        if (key.code === "Enter") { 
            Wordle(ConstructInputWord(myCellRow));
        }
    });
}

function Wordle(playerWord){
    tryCounter++; console.log("tryCounter = " + tryCounter);

    if(!ComprobarWin(playerWord)){
        if(ComprobarValidez(playerWord)){
            CompararLetra(playerWord);
        }
    }

    if(tryCounter < 5){
        myCellRow = AddRow();
    }
}

function ConstructInputWord(cellRow){
    let inputedWord = "";
    for (const iterator of cellRow) {
        inputedWord += iterator.firstChild.value;
        console.log("inputedWord: "+inputedWord);
    }
    return inputedWord;
}
function AddRow (){
    let cellRow = [];
    let inputedWord = "eo";
    let row = myTable.insertRow(tryCounter);
    for(let i = 0; i < 5; i++){
        let myCell = row.insertCell(i);
        myCell.innerHTML = '<input type="text" class="cell'+tryCounter+'" maxlength="1" size="5">';
        cellRow.push(myCell);
        AddMyEventListener(myCell);
    }
    return cellRow;
}


function ComprobarWin(playerWord){
    if (playerWord == palabraSecreta) {
        console.log("Victory");
        return true;
    }else{
        console.log("Defeat");
        return false;
    }
}
function ComprobarValidez(playerWord){
    if(diccionario.find(Element => Element === playerWord) !== undefined)
        return true;
    else{
        console.log("no válida");
        return false;
    }
}
function CompararLetra(playerWord){
    let correction = [];
    for (let key in playerWord){
        if(palabraSecreta.includes(playerWord[key])){
            if(CompararLetraExacta(key, playerWord)){
                correction.push(2);
            }else
            correction.push(1);
        } else
        correction.push(0);
    }
    console.log(correction);
    return correction;
    // 0 = letra no encontrada
    // 1 = letra en diferente posicion
    // 2 = letra en posicion exacta
}
function CompararLetraExacta(index, playerWord){
    if(playerWord[index] === palabraSecreta[index])
        return true;
    else
        return false;
}